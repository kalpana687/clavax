import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { StudentService } from '../student.service'
@Component({
  selector: 'app-basicinfo',
  templateUrl: './basicinfo.component.html',
  styleUrls: ['./basicinfo.component.scss']
})


export class BasicinfoComponent implements OnInit {
  
  basicInfo: FormGroup;
  constructor(public router: Router, private formBuilder: FormBuilder, private studentService: StudentService) { }

  ngOnInit() {
    this.createBasicForm();
    this.basicInfo.get('gender').valueChanges.subscribe(val => {
      console.log(val, typeof(val))
      if(val=='3'){
        console.log(val, "Entered")
      this.basicInfo.controls['other'].setValidators([Validators.required, Validators.maxLength(12), Validators.pattern('[a-zA-Z]+')])
     this.basicInfo.controls['other'].updateValueAndValidity()
     console.log(this.basicInfo.controls, "controls");
      }else{
        this.basicInfo.controls['other'].clearValidators()
        this.basicInfo.controls['other'].updateValueAndValidity()
      }
    })
  }

  createBasicForm() {
    this.basicInfo = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(56), Validators.pattern('[a-zA-Z]+')]],
      email: ['', [Validators.required, Validators.maxLength(112), Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      mobile: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^[1-9][0-9]{9}$/)]],
      dob: ['', [Validators.required, Validators.maxLength(10)]],
      other: [''], 
      gender: ['', [Validators.required]]
    });
  }

  public findInvalidControls(form) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }    
    return invalid;    
}

  submit(data) { 
    // this.findInvalidControls(this.basicInfo)   
    if(this.basicInfo.valid){    
    let opts = {
      name : data.name,
      email : data.email,
      mobile : data.mobile,
      dob : data.dob,
      other : data.other,
      gender : data.gender
    }     
    this.studentService.addStudentBasicInfo(opts).subscribe(data => {

    },error =>{

    })
    this.router.navigate(['address'])
  }
  }
}
