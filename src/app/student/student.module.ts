import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { BasicinfoComponent } from './basicinfo/basicinfo.component';
import { AddressinfoComponent } from './addressinfo/addressinfo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';
//import { RouterModule} from "@angular/router";
import { MatButtonModule } from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { StudentService} from './student.service'
import { HttpClientModule } from "@angular/common/http";

import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [BasicinfoComponent, AddressinfoComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatGridListModule,

    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule
  ],
  providers: [
    StudentService
  ]
})
export class StudentModule { }
