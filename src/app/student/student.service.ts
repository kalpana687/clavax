import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private httpClient : HttpClient) { }

  addStudentBasicInfo(opts){
    return this.httpClient.post(environment.apiUrl + "/basicInfo", opts)
  }

  addAddressInfo(opts, id){
    return this.httpClient.put(environment.apiUrl + "/addressInfo/" + id, opts)
  }

}
