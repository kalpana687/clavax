import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { StudentService} from "../student.service";

@Component({
  selector: 'app-addressinfo',
  templateUrl: './addressinfo.component.html',
  styleUrls: ['./addressinfo.component.scss']
})
export class AddressinfoComponent implements OnInit {

  public id: string = "";
  addressInfo: FormGroup;

  constructor(private formBuilder: FormBuilder, private studentService: StudentService) { }

  ngOnInit() {
    this.createAddressForm()
  }

  createAddressForm() {
    this.addressInfo = this.formBuilder.group({
      address: ['', [Validators.required, Validators.maxLength(256)]],
      additional: ['', [Validators.maxLength(256)]],
      pinCode: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(/^[1-9][0-9]{5}$/)]],
      city: ['', [Validators.required, Validators.maxLength(56)]],
      state: ['', [Validators.required, Validators.maxLength(56)]],
    });
  }

  submit(opts) {
    if(this.addressInfo.valid){
    let address = {
      address : opts.address,
      additional : opts.additional,
      pinCode : opts.pinCode,
      city : opts.city,
      state : opts.state
    }
    this.studentService.addAddressInfo(address,this.id).subscribe(opts => {
    },error =>{

    })
  }
  }
}
