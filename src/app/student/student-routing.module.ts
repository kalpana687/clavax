import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressinfoComponent } from './addressinfo/addressinfo.component';
import { BasicinfoComponent } from "./basicinfo/basicinfo.component";
const routes: Routes = [
  {
    path: '',
    component: BasicinfoComponent
  },
  {
    path: 'address',
    component: AddressinfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
